import { DATA_LOADED } from "../constants";

const initialState = {
    wordItems: []
};

function rootReducer(state = initialState, action) {
    if (action.type === DATA_LOADED) {
        return Object.assign({}, state, {
            wordItems: action.payload
        });
    }

    return state;
}

export default rootReducer;