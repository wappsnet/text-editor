import {
    DATA_LOADED,
    DATA_REQUESTED,
    WORDS_REQUEST_URL
} from "../constants";

export function getData() {
    return { type: DATA_REQUESTED };
}

export function getWords(searchText) {
    return function(dispatch) {
        return fetch(WORDS_REQUEST_URL + searchText)
            .then(response => response.json())
            .then(json => {
                dispatch({
                    type: DATA_LOADED,
                    payload: json
                });
            });
    };
}