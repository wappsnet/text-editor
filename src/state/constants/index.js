export const DATA_LOADED = "DATA_LOADED";
export const DATA_REQUESTED = "DATA_REQUESTED";
export const WORDS_REQUEST_URL = "https://api.datamuse.com/words?ml=";

export const FORMAT_TOOLS = {
    bold: {
        name: 'bold',
        icon: 'B'
    },
    italic: {
        name: 'italic',
        icon: 'I'
    },
    underline: {
        name: 'underline',
        icon: 'U'
    },
    indent: {
        name: 'indent',
        icon: 'indent'
    },
    outdent: {
        name: 'outdent',
        icon: 'outdent'
    }
};