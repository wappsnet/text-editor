export function rgbToHex(rgbColor) {

    rgbColor = rgbColor.toString();

    if (!rgbColor || rgbColor.indexOf("rgb") < 0) {
        return;
    }

    if (rgbColor.charAt(0) === "#") {
        return rgbColor;
    }

    const nums = /(.*?)rgb\((\d+),\s*(\d+),\s*(\d+)\)/i.exec(rgbColor),
        r = parseInt(nums[2], 10).toString(16),
        g = parseInt(nums[3], 10).toString(16),
        b = parseInt(nums[4], 10).toString(16);

    return "#"+ (
        (r.length === 1 ? "0"+ r : r) +
        (g.length === 1 ? "0"+ g : g) +
        (b.length === 1 ? "0"+ b : b)
    );
}