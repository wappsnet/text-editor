export function getFormats(options = [
    'bold',
    'italic',
    'underline',
    'indent',
    'outdent'
]) {
    const formattingData = {};

    options.map((option) => formattingData[option] = document.queryCommandState(option));

    return formattingData;
}