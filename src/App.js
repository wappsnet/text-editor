import React from 'react';
import './App.scss';
import Editor from "./components/editor";

function App() {
  return (
      <div className="App light-mode">
          <Editor />
      </div>
  );
}

export default App;
