import React from 'react';
import {getFormats} from "../helpers/format";

export default function Toolbar(props) {
    const formats = getFormats(Object.keys(props.tools));

    return (
        <div className="toolbar">
            {Object.entries(props.tools).map(([key, tool]) => {
                return (
                    <button key={key}
                            className={"toolbar-button" + (formats[key] ? " active" : "")}
                            onClick={(e) => props.clickTool(e, key)}>
                        {tool.icon}
                    </button>
                )
            })}

            <input className="color-input"
                   type="color"
                   value={props.color || "#0000"}
                   onChange={(e) => props.clickColor(e.target.value)}/>

            {props.words  && (
                <select className="select-words"
                        onChange={(e) => props.selectWord(e.target.value)}>
                    {props.words.map((item, key) => (
                            <option key={key}
                                    value={item.word}>
                                {item.word}
                            </option>
                        )
                    )}
                </select>
            )}
        </div>
    )

}