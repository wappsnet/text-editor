import React, { PureComponent } from 'react';
import Toolbar from "./tools";

import { connect } from "react-redux";
import { getWords } from "../state/actions";
import { rgbToHex } from "../helpers/color";
import {
    getSelectionText
} from "../helpers/range";

import {FORMAT_TOOLS} from "../state/constants";

class Editor extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            content: props.content,
            selection: null,
            wordItems: props.wordItems
        };
    }

    handleKeyUp() {
        const selection = getSelectionText();

        this.setState({
            selection: selection
        });
    }

    handleSelect() {
        this.setState({
            selection: getSelectionText()
        }, () => {
            this.props.getWords(this.state.selection);
        });
    }

    clickTool(event, toolName) {
        document.execCommand(toolName);

        this.setState({
            selection: getSelectionText()
        });
    }

    clickColor(color) {
        document.execCommand('foreColor',false, color);

        this.setState({
            selection: getSelectionText()
        });
    }

    selectWord(word) {
        document.execCommand( 'insertText', true, word);

        this.setState({
            selection: getSelectionText()
        });
    }

    render() {
        return (
            <div className="editor-container">
                <div id="editor"
                     className="editor-content"
                     contentEditable={true}
                     onSelect={this.handleSelect.bind(this)}>
                    {this.state.content}
                </div>

                <div className={"editor-toolbar" + (this.state.selection ? " active" : "")}>
                    <Toolbar
                        clickTool={this.clickTool.bind(this)}
                        clickColor={this.clickColor.bind(this)}
                        selectWord={this.selectWord.bind(this)}
                        color={rgbToHex(document.queryCommandValue("foreColor"))}
                        words={this.props.wordItems}
                        tools={FORMAT_TOOLS}
                    />
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        wordItems: state.wordItems
    };
}

export default connect(mapStateToProps, { getWords })(Editor);
